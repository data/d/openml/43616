# OpenML dataset: Detailed-Analysis-on-campus-recruitment

https://www.openml.org/d/43616

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This data set consists of Placement data, of students in a XYZ campus. It includes secondary and higher secondary school percentage and specialisation. It also includes degree specialisation, type and Work experience and salary offers to the placed students
we will Analyse what factors are playing a major role in order to select a candidate for job recruitment

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43616) of an [OpenML dataset](https://www.openml.org/d/43616). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43616/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43616/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43616/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

